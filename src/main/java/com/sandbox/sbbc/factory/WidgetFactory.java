package com.sandbox.sbbc.factory;

import com.sandbox.sbbc.model.Widget;
import org.springframework.stereotype.Component;

@Component
public class WidgetFactory {

    public Widget generateStandardWidget() {
        Widget widget = new Widget();
        widget.setModel("T");
        widget.setName("Foo");
        widget.setValue("3.14");
        return widget;
    }
}
