package com.sandbox.sbbc.controller;

import com.sandbox.sbbc.factory.WidgetFactory;
import com.sandbox.sbbc.model.Widget;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WidgetController {

    @Autowired
    private WidgetFactory widgetFactory;

    @RequestMapping("/widget/standard")
    public Widget retrieveStandardWidget() {
        return widgetFactory.generateStandardWidget();
    }
}
