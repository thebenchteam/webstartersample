package com.sandbox.sbbc.controller;

import com.sandbox.sbbc.factory.WidgetFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/meta")
public class MetaDataController {

    @Value("${app.version}")
    private String version;

    @RequestMapping("/version")
    public String getVersion() {
        return version;
    }
}
